# README #

English number speller

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How to set up? ###

- Ruby

* For testing run `bundle install` to install rspec and:
`bin/rspec --format doc` to run the tests

### How to use? ###

- As a library: Just require the speller.rb file in your code and use the Speller class. Ex:

<pre><code>

    require './speller'
    speller = Speller.new
    puts Speller.spell 123456

</code></pre>

- As a tool: You can run `speller` as an unix program. Ex: `echo 123456 | ./speller`

https://asciinema.org/a/WtcGfVyfe0AJS660RIzkodeI5