# http://blog.nicksieger.com/articles/2007/10/06/obscure-and-ugly-perlisms-in-ruby/

require './speller'

speller = Speller.new

#!/usr/bin/env ruby -i

Header = DATA.read

ARGF.each_line do |e|
    puts speller.spell Header if ARGF.pos - e.length == 0
end

__END__
