# The algorithm used in this Class is based on the possible powers of 10
# for all the numbers in the Z+ set, you should be able to compute the spelling
# of any number that can be stored on memory... or your imagination :)
# https://en.wikipedia.org/wiki/Power_of_10

class Speller
  # TODO: Feed powers of 10 names from wikipedia or an API
  POWERS = {
      17 => 'hundred {quadrillion}',
      15 => 'quadrillion',
      14 => 'hundred {trillion}',
      12 => 'trillion',
      11 => 'hundred {billion}',
      9 => 'billion',
      8 => 'hundred {million}',
      6 => 'million',
      5 => 'hundred {thousand}',
      3 => 'thousand',
      2 => 'hundred',
      1 => '',
      0 => ''
  }

  UNITS = {
    0 => 'zero',
    1 => 'one',
    2 => 'two',
    3 => 'three',
    4 => 'four',
    5 => 'five',
    6 => 'six',
    7 => 'seven',
    8 => 'eight',
    9 => 'nine'
  }

  DECA_HECTO = {
    10 => 'ten',
    11 => 'eleven',
    12 => 'twelve',
    13 => 'thirteen',
    14 => 'fourteen',
    15 => 'fifteen',
    16 => 'sixteen',
    17 => 'seventeen',
    18 => 'eighteen',
    19 => 'nineteen',
    20 => 'twenty',
    30 => 'thirty',
    40 => 'forty',
    50 => 'fifty',
    60 => 'sixty',
    70 => 'seventy',
    80 => 'eighty',
    90 => 'ninety'
  }

  private
    # Strategy pattern in order to translate an Integer -which
    # nearest power of 10 is lower than 2- to its string equivalent
    def to_str number
      unless number == 0
        digits = digits number

        if digits.size == 1
          UNITS[number]
        else
          last_digit = number % 10
          if last_digit == 0 || number < 20
            DECA_HECTO[number]
          elsif number > 19
            [
              DECA_HECTO[number - last_digit],
              UNITS[last_digit]
            ].join '-'
          end
        end
      else
        '' # Zero is not spelled in composite numbers
      end
    end

    # Get a number's digits
    def digits number 
      temp = number
      digits = [ ]

      begin
        digits.push temp % 10
        temp = temp / 10
      end while temp >= 1

      digits.reverse
    end

    # Find the nearest power of 10 for a given number
    def nearest_power number
      count = digits(number).size
      nearest = POWERS.detect { |power, suffix| power <= count -1 }

      nearest[0]
    end

    # Visitor pattern in order to traverse the splitted numbers tree
    def tree_walk tree, &visitor
      nodes = tree[:children]

      return if nodes.nil?

      nodes.each do |split|
        yield split if block_given?
      end

      nodes.each do |split|
       tree_walk split, &visitor
      end
    end

    # Splits the number (tree) until its name can be evaluated
    # i.e: its nearest power of 10 is lower than 2
    def split node
      number = node[:value]
      power = nearest_power number

      if power > 1
        node[:power_name] = POWERS[power]

        digits = digits(number)
        count = digits.size

        left_digits = digits[0 .. count - power - 1]
        left_number = left_digits.join ''

        right_digits = digits[count - power .. count]
        right_number = right_digits.join ''

        left_child_node = { 
          :value => Integer(left_number, 10),
          :power_name => POWERS[power],
          :children => [ ]
        }

        if POWERS[power].include? '{'
          if right_number.start_with? '00'
            left_child_node[:power_name] = POWERS[power].gsub(/[{}]/, '')
          else
            left_child_node[:power_name] = POWERS[power].gsub(/ {\w+}/, '')
          end
        end

        node[:children].push left_child_node
        split left_child_node

        if right_number.size > 0 && Integer(right_number, 10) > 0
          right_number = Integer right_number, 10
          right_power = nearest_power right_number

          right_child_node = { 
            :value => right_number,
            :power_name => POWERS[right_power],
            :children => [ ]
          }

          node[:children].push right_child_node

          split right_child_node
        end
      end
    end

  public

    # Spells out a given number
    def spell number
      begin
        number = Integer number
      rescue ArgumentError => e
        raise ArgumentError.new 'The input provided was not a number'
      end

      @tree  = {
          :value => number,
          :children => [ ]
      }
      split @tree

      if @tree[:children].size == 0
        return "#{to_str @tree[:value]} #{@tree[:power_name]}".strip
      end

      spelled = ''
      tree_walk @tree do |split|
        if split[:children].size == 0
          spelled = spelled.concat "#{to_str split[:value]} #{split[:power_name]} "
        end
      end
      spelled.strip
    end
end
