# If you want to test this algorithm deeper, you can write more tests by grabbing
# the values from this service: https://www.tools4noobs.com/online_tools/number_spell_words/
# It uses the Numbers_Words library for PHP

require 'spec_helper'
require './speller'

describe Speller do
  before :all do
    @speller = Speller.new
  end

  describe '#spell' do
    context 'tera' do
      it 'should spell out a trillion quantity correctly' do
        expect(@speller.spell 1820986120496).to eql 'one trillion eight hundred twenty billion nine hundred eighty-six million one hundred twenty thousand four hundred ninety-six'
      end
    end

    context 'giga' do
      it 'should spell out a billion quantity correctly' do
        expect(@speller.spell 127444165181).to eql 'one hundred twenty-seven billion four hundred forty-four million one hundred sixty-five thousand one hundred eighty-one'
      end
    end

    context 'mega' do
      it 'should spell out a million quantity correctly' do
        expect(@speller.spell 142350000).to eql 'one hundred forty-two million three hundred fifty thousand'
      end
    end

    context 'kilo' do
      it 'should spell out a thousand quantity correctly' do
        expect(@speller.spell 350008).to eql 'three hundred fifty thousand eight'
      end
    end

    context 'hecto' do
      it 'should spell out a hundred quantity correctly' do
        expect(@speller.spell 945).to eql 'nine hundred forty-five'
      end
    end

    context 'deca' do
      it 'should spell out a tens quantity correctly' do
        expect(@speller.spell 86).to eql 'eighty-six'
      end
    end

    context 'unit' do
      it 'should spell out a unit quantity correctly' do
        expect(@speller.spell 7).to eql 'seven'
      end
    end
  end
end
